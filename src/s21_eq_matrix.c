#include "s21_matrix.h"

int s21_eq_matrix(matrix_t *A, matrix_t *B) {
  int ret = SUCCESS, checkA = s21_check_matrix(A), checkB = s21_check_matrix(B);
  if (checkA || checkB) return 0;
  if (A->rows != B->rows || A->columns != B->columns) {
    ret = FAILURE;
  } else {
    for (int i = 0; i < A->rows && ret; i++) {
      for (int j = 0; j < A->columns; j++) {
        if (s21_fabs(A->matrix[i][j] - B->matrix[i][j]) >= 0.0000001)
          ret = FAILURE;
      }
    }
  }
  return ret;
}
