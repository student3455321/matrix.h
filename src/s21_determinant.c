// #include "s21_matrix.h"

// int s21_determinant(matrix_t *A, double *result) {
//   int checkA = s21_check_matrix(A);
//   if (checkA) return 1;
//   if (!is_square(A)) return 2;
//   *result = 1;
//   int swap_number = 0;
//   matrix_t temp = {0}, temp2 = {0};
//   s21_create_matrix(A->rows, A->columns, &temp);
//   s21_sum_matrix(A, &temp, &temp2);
//   for (int curr_diagonal_item = 0; curr_diagonal_item < A->rows - 1;
//        curr_diagonal_item++) {
//     int max_elem = find_max_in_row(A, curr_diagonal_item);
//     if (curr_diagonal_item != max_elem) {
//       swap_column(&temp, curr_diagonal_item, max_elem);
//       swap_number++;
//     }
//     for (int cur_row = curr_diagonal_item + 1; cur_row < temp.columns;
//          cur_row++) {
//       double coef_for_this_row =
//           temp.matrix[cur_row][curr_diagonal_item] /
//           temp.matrix[curr_diagonal_item][curr_diagonal_item];
//       for (int iter_column = curr_diagonal_item; iter_column < A->columns;
//            iter_column++) {
//         temp.matrix[cur_row][iter_column] -=
//             temp.matrix[curr_diagonal_item][iter_column] * coef_for_this_row;
//       }
//     }
//   }
//   for (int diag = 0; diag < A->columns; diag++) {
//     *result *= temp.matrix[diag][diag];
//   }
//   s21_remove_matrix(&temp);
//   s21_remove_matrix(&temp2);
//   if (swap_number % 2) {
//     *result *= -1;
//   }
//   return 0;
// }

#include "s21_matrix.h"

int s21_determinant(matrix_t *A, double *result) {
  int ret = 0, checkA = s21_check_matrix(A);
  if (checkA != 0) {
    ret = checkA;
  }
  if (ret != 0 || A->rows != A->columns) {
    ret = ret ? ret : 2;
  } else {
    *result = 1;
    int swap_number = 0;
    matrix_t temp = {0}, temp_cringe = {0};
    s21_create_matrix(A->rows, A->columns, &temp_cringe);
    s21_sum_matrix(A, &temp_cringe, &temp);
    s21_remove_matrix(&temp_cringe);
    for (int curr_diagonal_item = 0; curr_diagonal_item < A->rows - 1;
         curr_diagonal_item++) {
      int max_elem = find_max_in_row(A, curr_diagonal_item);
      if (curr_diagonal_item != max_elem) {
        swap_column(&temp, curr_diagonal_item, max_elem);
        swap_number++;
      }
      for (int cur_row = curr_diagonal_item + 1; cur_row < temp.columns;
           cur_row++) {
        double coef_for_this_row =
            temp.matrix[cur_row][curr_diagonal_item] /
            temp.matrix[curr_diagonal_item][curr_diagonal_item];
        for (int iter_column = curr_diagonal_item; iter_column < A->columns;
             iter_column++) {
          temp.matrix[cur_row][iter_column] -=
              temp.matrix[curr_diagonal_item][iter_column] * coef_for_this_row;
        }
      }
    }
    for (int diag = 0; diag < A->columns; diag++) {
      *result *= temp.matrix[diag][diag];
    }
    s21_remove_matrix(&temp);
    if (swap_number % 2) {
      *result *= -1;
    }
  }
  return ret;
}
