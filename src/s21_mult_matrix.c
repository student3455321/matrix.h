#include "s21_matrix.h"

int s21_mult_matrix(matrix_t *A, matrix_t *B, matrix_t *result) {
  int checkA = s21_check_matrix(A), checkB = s21_check_matrix(B);
  if (checkA || checkB) return 1;
  if (A->columns != B->rows) return 2;
  if (s21_create_matrix(A->rows, B->columns, result)) return 2;
  for (int i = 0; i < result->rows; i++) {
    for (int j = 0; j < result->columns; j++) {
      for (int a = 0; a < A->columns; a++) {
        result->matrix[i][j] += A->matrix[i][a] * B->matrix[a][j];
      }
    }
  }
  return 0;
}