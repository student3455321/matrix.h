#include "s21_matrix.h"

int s21_create_matrix(int rows, int columns, matrix_t *result) {
  if (rows < 1 || columns < 1) {
    return 1;
  }
  int ret = 0;
  result->rows = rows;
  result->columns = columns;
  double *ptr = NULL;
  result->matrix =
      malloc(sizeof(double *) * rows + sizeof(double) * rows * columns);
  if (result->matrix != NULL) {
    memset(result->matrix, 0,
           sizeof(double *) * rows + sizeof(double) * rows * columns);
    ptr = (double *)(result->matrix + rows);
    for (int i = 0; i < rows; i++) {
      result->matrix[i] = (ptr + columns * i);
    }
    s21_matrix_reset(result);
  } else {
    ret = 1;
  }
  return ret;
}