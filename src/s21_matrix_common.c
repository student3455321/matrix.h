#include "s21_matrix.h"

long double s21_fabs(double x) {
  if (x <= 0) {
    x *= -1;
  }
  return (long double)x;
}

// возвращает индекс максимального элемента
int find_max_in_row(matrix_t *A, int curr) {
  int ret = curr;
  for (int i = curr; i < A->columns; i++) {
    if (fabs(A->matrix[curr][i]) > fabs(A->matrix[curr][ret])) {
      ret = i;
    }
  }
  return ret;
}

void swap_column(matrix_t *A, int index1, int index2) {
  for (int i = 0; i < A->rows; i++) {
    double temp = A->matrix[i][index1];
    A->matrix[i][index1] = A->matrix[i][index2];
    A->matrix[i][index2] = temp;
  }
}

int s21_check_matrix(matrix_t *A) {
  int error = 0;
  if (A == NULL || A->matrix == NULL || A->columns < 1 || A->rows < 1) {
    error = 1;
  }
  return error;
}

void s21_matrix_reset(matrix_t *A) {
  for (int i = 0; i < A->rows; i++) {
    for (int j = 0; j < A->columns; j++) {
      A->matrix[i][j] = 0;
    }
  }
}

bool same_size(matrix_t *A, matrix_t *B) {
  return A->columns == B->columns && A->rows == B->rows;
}

bool is_square(matrix_t *A) { return A->columns == A->rows; }