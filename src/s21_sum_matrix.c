#include "s21_matrix.h"

int s21_sum_matrix(matrix_t *A, matrix_t *B, matrix_t *result) {
  int checkA = s21_check_matrix(A), checkB = s21_check_matrix(B);
  if (checkA || checkB) return 1;
  if (!same_size(A, B)) return 2;
  if (s21_create_matrix(A->rows, A->columns, result)) return 1;
  for (int i = 0; i < A->rows; i++) {
    for (int j = 0; j < A->columns; j++) {
      result->matrix[i][j] = A->matrix[i][j] + B->matrix[i][j];
    }
  }
  return 0;
}