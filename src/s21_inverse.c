#include "s21_matrix.h"

int s21_inverse_matrix(matrix_t *A, matrix_t *result) {
  int checkA = s21_check_matrix(A);
  if (checkA) return 1;
  if (!is_square(A)) return 2;
  double deter = 0;
  s21_determinant(A, &deter);
  if (deter != 0) {
    matrix_t comps = {0}, trans = {0};
    s21_transpose(A, &trans);
    s21_calc_complements(&trans, &comps);
    s21_mult_number(&comps, 1. / deter, result);
    s21_remove_matrix(&trans);
    s21_remove_matrix(&comps);
  }
  return 0;
}
