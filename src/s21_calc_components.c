#include "s21_matrix.h"

void get_minor(matrix_t *A, matrix_t *result, int rows_exep, int column_exep) {
  for (int i = 0, a = 0; i < A->rows; i++) {
    for (int j = 0, b = 0; j < A->columns; j++) {
      if (i != rows_exep && j != column_exep) {
        result->matrix[a][b] = A->matrix[i][j];
        b++;
      }
    }
    if (i != rows_exep) {
      a++;
    }
  }
}

int s21_calc_complements(matrix_t *A, matrix_t *result) {
  int checkA = s21_check_matrix(A);
  if (checkA) return 1;
  if (!is_square(A)) return 2;
  if (s21_create_matrix(A->rows, A->columns, result)) return 1;
  for (int i = 0; i < A->rows; i++) {
    for (int j = 0; j < A->columns; j++) {
      matrix_t temp = {0};
      s21_create_matrix(A->rows - 1, A->columns - 1, &temp);
      get_minor(A, &temp, i, j);
      s21_determinant(&temp, &(result->matrix[i][j]));
      s21_remove_matrix(&temp);
    }
  }
  for (int x = 0; x < result->rows; x++) {
    for (int y = 0; y < result->columns; y++) {
      if ((x + y) % 2) {
        result->matrix[x][y] *= -1;
      }
    }
  }
  return 0;
}